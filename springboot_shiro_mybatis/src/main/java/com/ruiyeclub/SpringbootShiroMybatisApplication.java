package com.ruiyeclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootShiroMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootShiroMybatisApplication.class, args);
    }

}
