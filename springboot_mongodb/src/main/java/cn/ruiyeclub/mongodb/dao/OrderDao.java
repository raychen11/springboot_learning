package cn.ruiyeclub.mongodb.dao;

/**
 * @author Ray。
 * @create 2020-05-22 23:50
 */
import cn.ruiyeclub.mongodb.entity.Order;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface OrderDao extends MongoRepository<Order, Integer>{
    Order getByOrderNo(String orderNo);
    public Order getByOrderNoLike(String orderNo);
}