package cn.ruiyeclub.mongodb.service;

import cn.ruiyeclub.mongodb.entity.Order;

/**
 * @author Ray。
 * @create 2020-05-22 23:51
 */
public interface OrderService {
    public Order getByOrderNo(String orderNo);
    public Order getByOrderNoLike(String orderNo);
    public void saveOrder(Order order);
    public void removeOrderByOrderNo(String orderNo);
    public void updateOrder(Order order);
}