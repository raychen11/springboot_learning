package cn.ruiyeclub.mongodb.service.imp;

import cn.ruiyeclub.mongodb.dao.OrderDao;
import cn.ruiyeclub.mongodb.entity.Order;
import cn.ruiyeclub.mongodb.service.OrderService;

/**
 * @author Ray。
 * @create 2020-05-22 23:52
 */

import javax.annotation.Resource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService{

    @Resource
    private MongoTemplate mongoTemplate;

    @Resource
    private OrderDao orderDao;

    @Override
    public Order getByOrderNo(String orderNo) {
        return orderDao.getByOrderNo(orderNo);
    }

    @Override
    public void saveOrder(Order order) {
        orderDao.save(order);

    }

    @Override
    public Order getByOrderNoLike(String orderNo) {
        return orderDao.getByOrderNoLike(orderNo);
    }

    @Override
    public void removeOrderByOrderNo(String orderNo) {
        Query q=new Query(new Criteria("orderNo").is(orderNo));
        mongoTemplate.remove(q,Order.class);
    }

    @Override
    public void updateOrder(Order order) {
        Query q=new Query(new Criteria("orderNo").is(order.getOrderNo()));
        Update update=new Update().set("nickName", order.getNickName());
        mongoTemplate.updateMulti(q, update, Order.class);
    }

}