package cn.ruiyeclub.mongodb.entity;

/**
 * @author Ray。
 * @create 2020-05-22 23:32
 */
import org.springframework.data.annotation.Id;

public class Order {
    /**
     * 表示为主键
     */
    @Id
    private int id;
    private String orderNo;
    private String nickName;
    private double totalPrice;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

}