package cn.ruiyeclub.entity;
import lombok.Data;

/**
 * @author Ray。
 */
@Data
public class Result {
    private int code;
    private String message;
}