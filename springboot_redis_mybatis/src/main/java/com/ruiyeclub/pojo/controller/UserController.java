package com.ruiyeclub.pojo.controller;

import com.ruiyeclub.pojo.entity.User;
import com.ruiyeclub.pojo.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * (User)表控制层
 *
 * @author Ray。
 * @since 2020-02-19 18:24:28
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public User selectOne(Integer id) {
        return this.userService.queryById(id);
    }

    @GetMapping("selectAll")
    public List<User> selectAll(){
        List<User> users = userService.selectAll();
        return users;
    }

    /**
     * 使用多线程测试加了锁的方法
     */
    @GetMapping("/selectAll1")
    public List<User> selectAll1(){

        //写一个runnable线程 执行查询方法
        Runnable runnable=new Runnable(){
            @Override
            public void run() {
                userService.selectAll();
            }
        };

        //使用固定线程池，测试线程安全问题 25个线程 执行10000次任务
        ExecutorService executorService= Executors.newFixedThreadPool(25);
        for (int i=0;i<10000;i++){
            executorService.submit(runnable);
        }
        return userService.selectAll();
    }

}