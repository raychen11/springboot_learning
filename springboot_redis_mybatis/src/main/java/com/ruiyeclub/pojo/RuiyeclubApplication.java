package com.ruiyeclub.pojo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.ruiyeclub.controller","com.ruiyeclub.service.impl"})
@MapperScan("com.ruiyeclub.dao")
public class RuiyeclubApplication {

    public static void main(String[] args) {
        SpringApplication.run(RuiyeclubApplication.class, args);
    }

}
