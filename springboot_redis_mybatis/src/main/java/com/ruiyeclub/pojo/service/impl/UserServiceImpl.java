package com.ruiyeclub.pojo.service.impl;

import com.ruiyeclub.pojo.entity.User;
import com.ruiyeclub.pojo.dao.UserDao;
import com.ruiyeclub.pojo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author Ray。
 * @since 2020-02-19 18:24:28
 */
@Service("userService")
public class UserServiceImpl implements UserService {
    @Resource
    private UserDao userDao;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Integer id) {
        return this.userDao.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<User> queryAllByLimit(int offset, int limit) {
        return this.userDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User insert(User user) {
        this.userDao.insert(user);
        return user;
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        this.userDao.update(user);
        return this.queryById(user.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.userDao.deleteById(id) > 0;
    }

    @Override
    public List<User> selectAll() {
        //字符串序列号器
        RedisSerializer redisSerializer=new StringRedisSerializer();
        redisTemplate.setKeySerializer(redisSerializer);

        //*****************高并发条件下，此处有点问题，存在缓存穿透**************
        //查询缓存
//        List<User> userList= (List<User>) redisTemplate.opsForValue().get("allUsers");
//
//        if (null==userList){
//            //缓存为空，查询一遍数据库
//            userList=userDao.queryAll();
//            System.out.println("laile");
//            redisTemplate.opsForValue().set("allUsers",userList);
//        }
//        return userList;

        //************处理高并发问题，只适用于单体项目，分布式项目仍然存在问题************
        /**
         * 双重检测锁：
         * 解释一下：第一次从缓存里面拿，拿的到直接return，拿不到时，注意
         * 因为并发情况下，只允许第一个人查询数据库并存入redis。
         * 这里存在线程都在第一个if这里的情况，哪怕后面的人已经把数据存入缓存，其他线程依旧
         * 进入if判断里面，所以这里需要第二个if。
         */
        List<User> userList= (List<User>) redisTemplate.opsForValue().get("allUsers");

        if(null==userList){
            synchronized (this){
                //从redis获取一下
                userList=(List<User>)redisTemplate.opsForValue().get("allUsers");

                if(null==userList){
                    //缓存为空，查询一遍数据库
                    userList=userDao.queryAll();
                    System.out.println("查询的数据库...");
                    redisTemplate.opsForValue().set("allUsers",userList);
                }else {
                    //这里else只是为了测试
                    System.out.println("查询的缓存。。。");
                }
            }
        }else {
            System.out.println("查询的缓存。。。");
        }
        return  userList;
    }
}