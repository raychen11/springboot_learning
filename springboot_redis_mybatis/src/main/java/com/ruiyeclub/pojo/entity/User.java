package com.ruiyeclub.pojo.entity;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author Ray。
 * @since 2020-02-19 18:24:27
 */
public class User implements Serializable {
    private static final long serialVersionUID = 275961879455487786L;
    
    private Integer id;
    
    private String name;
    
    private String password;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}