package cn.ruiyeclub.wxpay.controller;

import cn.ruiyeclub.wxpay.service.WeChatPayService;
import cn.ruiyeclub.wxpay.utils.IpUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ray。
 * @create 2020-05-07 12:30
 */
@Api(tags = "第三方支付生成订单接口", description = "生成订单")
@RestController
@RequestMapping("/api/create")
public class WxPayController {
    @Autowired
    private WeChatPayService wxPayService;

    @RequestMapping(value = "/wxpay/create", method = {RequestMethod.POST, RequestMethod.GET})
    public Map wxNotify(HttpServletRequest request, HttpServletResponse response){
        String ip=IpUtils.getIpAddr(request);
        String orderId="20150806125346201508061253462212"; //32位
        BigDecimal price=new BigDecimal("0.01");
        String body="test";
        Map map=new HashMap();
        try {
            return wxPayService.createOrder(orderId,price,body,ip);
        } catch (IOException e) {
            response.setHeader("Content-type", "application/xml");
            return map;
        }
    }


}