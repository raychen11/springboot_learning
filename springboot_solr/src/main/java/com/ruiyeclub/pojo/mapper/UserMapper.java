package com.ruiyeclub.pojo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruiyeclub.pojo.pojo.User;

public interface UserMapper extends BaseMapper<User> {
}
