package com.ruiyeclub.pojo.controller;

import com.ruiyeclub.pojo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("add")
    public void addSolr(){
        userService.addSolr();
    }

}
