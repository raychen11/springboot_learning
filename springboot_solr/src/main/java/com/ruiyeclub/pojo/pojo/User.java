package com.ruiyeclub.pojo.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.io.Serializable;

@Data
@SolrDocument
public class User implements Serializable {

    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String name;
    private String password;
}
