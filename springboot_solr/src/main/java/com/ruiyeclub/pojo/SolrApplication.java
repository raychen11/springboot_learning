package com.ruiyeclub.pojo;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.ruiyeclub.service","com.ruiyeclub.config","com.ruiyeclub.controller"})
@MapperScan("com.ruiyeclub.mapper")
@EnableCaching
public class SolrApplication {

    public static void main(String[] args) {
        SpringApplication.run(SolrApplication.class,args);
    }
}
