一.环境搭建
  1. jdk环境变量
  2. 从官网下载solr解压
     https://lucene.apache.org/solr/downloads.html
  3.启动服务
    在solr/bin目录执行
     solr start
  4.测试,在地址栏
     http://127.0.0.1:8983/solr/
  5.创建core
     a)网页创建
     b)在bin用命令创建
       solr create -c ruiyeclub
  6.中文分词
     a) 复制中文分词到D:\solr-7.7.2\server\solr-webapp\webapp\WEB-INF\lib
     b) 编辑刚才创建的core目录ruiyeclub
        D:\solr-7.7.2\server\solr\ruiyeclub\conf里面managed-schema文件
	<fieldType name="text_ik" class="solr.TextField">
             <analyzer class="org.wltea.analyzer.lucene.IKAnalyzer"/>
        </fieldType>
     c) 重启solr
         solr stop all
	 solr start
     d)  http://127.0.0.1:8983/solr/ruiyeclub

二.开发
   1.开发包pom
      <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-solr</artifactId>
        </dependency>
   2.修改配置文件(可选)application.properties
     spring.data.solr.host=http://127.0.0.1:8983/solr/

   3.创建配置类
     @Configuration
	public class SolrTemplateConfig {

	    @Bean
	    public SolrTemplate solrTemplate(SolrClient solrClient) {
		return new SolrTemplate(solrClient);
	    }
	}
   4.在do/po/vo类上面加注解（需要搜索类,一般是商品。。。)
     @SolrDocument(solrCoreName = "sunjob")
     public class Dep implements  java.io.Serializable  {

    @TableId(type = IdType.AUTO)
    @Field("id")
    private  Integer depId;


    @Field("depName")
    private  String  depName;

    5.需要用solr类里面注入
      @Autowired
      private SolrTemplate solrTemplate;


      List list = super.list();
      solrTemplate.saveBeans("ruiyeclub",list);
      solrTemplate.commit("ruiyeclub");