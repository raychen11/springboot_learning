package cn.ruiyeclub.entity;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

@Data
public class User {
    @Null
    private Long id;
    @NotBlank
    private String name;
    @Email
    private String email;

}
