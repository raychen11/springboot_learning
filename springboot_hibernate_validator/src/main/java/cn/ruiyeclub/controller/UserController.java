package cn.ruiyeclub.controller;

import cn.ruiyeclub.entity.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Ray。
 * @create 2020-05-31 13:18
 */
@RestController
public class UserController {

    @PostMapping("/register")
    public Map<String, Object> register(@RequestBody @Valid User user){
        // 你自己的代码
        Map map = new HashMap<>();
        map.put("user",user);
        return map;
    }


}