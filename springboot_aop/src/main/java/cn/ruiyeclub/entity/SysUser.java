package cn.ruiyeclub.entity;

import java.io.Serializable;

/**
 * (SysUser)实体类
 *
 * @author Ray。
 * @since 2020-06-07 22:04:09
 */
public class SysUser implements Serializable {
    private static final long serialVersionUID = 385199563978865675L;
    
    private Integer id;
    
    private String userName;
    
    private Integer userAge;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserAge() {
        return userAge;
    }

    public void setUserAge(Integer userAge) {
        this.userAge = userAge;
    }

}